﻿// Lesson_17.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>
using namespace std;

template<typename T>
class Stack
{
private:
	int NumArray = 0;
	T* StackArray = new T[NumArray];


public:
	Stack()
	{

	}
	//Добавляет элемент
	void Push(T Value)
	{
		T* NewArray = new T[NumArray + 1];
		cout << "Добавлен элемент " << Value << endl;

		for (int i = 0; i < NumArray; i++)
		{
			NewArray[i] = StackArray[i];
		}
		NewArray[NumArray] = Value;
		delete[] StackArray;
		NumArray++;
		StackArray = NewArray;
	}
	//Удаляет последний элемент
	void Del()
	{
		cout << "Удалён последний элемент " << endl;
		T* NewArray = new T[NumArray - 1];
		for (int i = 0; i < NumArray - 1; i++)
		{
			NewArray[i] = StackArray[i];;
		}
		delete[] StackArray;
		NumArray--;
		StackArray = NewArray;
	}

	//Достать верхний элемент из стека
	T Pop()
	{
			return StackArray[NumArray-1];
	}
	
	//Печатает массив
	void Print()
	{
		for (int i = 0; i < NumArray; i++)
		{

			cout << *(StackArray + i) << "   ";
		}

		cout << endl;
	}
	//Чистим память
	~Stack()
	{
		delete[] StackArray;
	}

};



int main()
{
	setlocale(LC_ALL, "ru");
	Stack<int>MyStackInteger;
	Stack<double> MystackDouble;
	Stack<string> MystackString;

	//Cтек int

	cout << "Добавляет в стек int" << endl;
	MyStackInteger.Push(12);
	MyStackInteger.Print();

	MyStackInteger.Push(23);
	MyStackInteger.Print();

	MyStackInteger.Push(44);
	MyStackInteger.Print();
	
	MyStackInteger.Del();
	MyStackInteger.Print();

	cout << "Последний элемент  " << MyStackInteger.Pop() << endl;
	cout << endl<<endl;
	

	//Cтек double
	cout << "Добавляет в стек double" << endl;
	MystackDouble.Push(12.32);
	MystackDouble.Print();
	
	MystackDouble.Push(15.25);
	MystackDouble.Print();

	MystackDouble.Del();
	MystackDouble.Print();
	cout << endl << endl;
	
	//Cтек String
	cout << "Добавляет в стек String" << endl;
	MystackString.Push("One");
	MystackDouble.Print();
	
	MystackString.Push("two");
	MystackString.Print();
	
	MystackString.Push("three");
	MystackString.Print();

	MystackString.Del();
	MystackString.Print();
	cout << "Последний элемент  " << MystackString.Pop() << endl;
	cout << endl << endl;




	return 0;

}